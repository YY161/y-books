from django import template

register = template.Library()

def rate_class(value):
    class_name = int(value)

    if value % 1 >= 0.5:
        class_name += 1
    elif value % 1 != 0:
        class_name = str(class_name) + '-5'

    return class_name

register.filter('rating', rate_class)
