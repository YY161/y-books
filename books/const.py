CATEGORY_SLIDER_PREFIX = [
    'comic',
    'magazine',
    'novel',
    'life',
    'business',
]

CATEGORY_SLIDER_TITLE = [
    '漫画',
    '雑誌',
    '小説',
    '生活',
    'ビジネス',
]

CATEGORY_SLIDER_IMAGE = [
    'https://placehold.jp/ffa500/ff8c00/300x400.png?text=COMIC',
    'https://placehold.jp/ff4500/dc143c/300x400.png?text=MAGAZINE',
    'https://placehold.jp/6a5acd/da70d6/300x400.png?text=NOVEL',
    'https://placehold.jp/3cb371/2e8b57/300x400.png?text=LIFE',
    'https://placehold.jp/191970/4169e1/300x400.png?text=BUSINESS',
]
