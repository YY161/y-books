from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Product, Comment
from django.views.generic import CreateView
from django.core.paginator import Paginator
from django.db.models import Count, Avg
from .forms import ProductForm
from django_seed import Seed
from .const import *
import random

def index(request):
    popular_books = Product.objects.all()[:3]

    category_books = Product.objects.filter(category__lt=5)
    category_book_list = [category_books.filter(category=category)[:12] for category in range(5)]

    return render(request, 'books/index.html', {
        'popular_books': popular_books,
        'category_slider_elements': zip(
            category_book_list,
            CATEGORY_SLIDER_PREFIX,
            CATEGORY_SLIDER_TITLE,
            CATEGORY_SLIDER_IMAGE
        )
    })

def detail(request, id):
    book = get_object_or_404(Product, pk=id)
    comments = book.comment_set.all()
    rate_average = comments.aggregate(Avg('rate'))['rate__avg']

    return render(request, 'books/detail.html', {
        'book': book,
        'comments': comments,
        'rate_average': rate_average,
    })

def search(request):
    search_result = []
    search_text = request.GET.get('text')
    category = request.GET.get('category')
    price = request.GET.get('price')
    rate = request.GET.get('rate')

    if search_text:
        in_title_description = Product.objects.all().in_title_description(search_text)
        in_title_books = Product.objects.all().in_title(search_text)
        in_description_books = Product.objects.all().in_description(search_text)

        search_result = in_title_description.union(
            in_title_books,
            in_description_books
        ).order_score()
    else:
        search_result = Product.objects.all().rate_average()

    if category:
        search_result = search_result.filter(category=category)

    if price:
        search_result = search_result.price_filter(price)

    if rate:
        search_result = search_result.rate_filter(rate)

    paginate_books = Paginator(search_result, 20)
    page = request.GET.get('page')
    contents = paginate_books.get_page(page)

    return render(request, 'books/search.html', {
        'books': contents,
    })

class AddBookView(CreateView):
    model = Product
    form_class = ProductForm
    template_name = "books/add_book.html"

def seed(request):
    Product.objects.all().delete()

    seeder = Seed.seeder()

    seeder.add_entity(Product, 100, {
        'price' : lambda x: random.randint(500, 5000),
        'category': lambda x: random.randrange(6),
        'page_count': lambda x: random.randint(100, 400),
    })

    seeder.execute()

    products = Product.objects.all()

    for product in products:
        for _ in range(5):
            product.comment_set.create(
                title = seeder.faker.name(),
                rate = random.randint(1, 5),
                body = seeder.faker.text(),
            )

    return redirect('index')
