from django.contrib import admin

# Register your models here.
from .models import Product, Comment

class CommentInline(admin.TabularInline):
    model = Comment

class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'price', 'published_at', 'author', 'page_count', 'description', 'category')
    inlines = [
        CommentInline,
    ]

class CommentAdmin(admin.ModelAdmin):
    list_display = ('title', 'rate', 'body')

admin.site.register(Product, ProductAdmin)
admin.site.register(Comment, CommentAdmin)
