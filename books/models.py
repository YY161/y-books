from django.db import models
from django.db.models import Count, Avg, F, Q
from django.core.validators import MaxValueValidator, MinValueValidator
from django.urls import reverse

BOOK_CATEGORY_CHOICE = (
    (0, "漫画"),
    (1, "雑誌"),
    (2, "小説"),
    (3, "生活"),
    (4, "ビジネス"),
    (5, "教育"),
    (6, "未定"),
    (7, "未定"),
    (8, "未定"),
    (9, "未定"),
)

PRICE_RANGE = (
    (0, 1000),
    (1000, 3000),
    (3000, 5000),
)

RATE_RANGE = (
    (5.1, 4.0),
    (4.0, 3.0),
    (3.0, 0.0),
)

class ProductQuerySet(models.QuerySet):
    def rate_average(self):
        return self.annotate(rate__avg=Avg('comment__rate'))

    def add_rate_average(self, score):
        return self.annotate(rate__avg=Avg('comment__rate'), score=F('rate__avg') + score)

    def in_title_description(self, text):
        return self.filter(title__contains=text, description__contains=text).add_rate_average(20)

    def in_title(self, text):
        return self.filter(title__contains=text).exclude(description__contains=text).add_rate_average(10)

    def in_description(self, text):
        return self.filter(description__contains=text).exclude(title__contains=text).add_rate_average(0)

    def order_score(self):
        return self.order_by('-score', '-published_at')

    def price_filter(self, price):
        price_range = PRICE_RANGE[int(price)]
        return self.filter(price__gte=price_range[0], price__lte=price_range[1])

    def rate_filter(self, rate):
        rate_range = RATE_RANGE[int(rate)]
        return self.filter(rate__avg__lt=rate_range[0], rate__avg__gte=rate_range[1])

class ProductManager(models.Manager):
    def get_queryset(self):
        return ProductQuerySet(self.model, using=self._db)

class Product(models.Model):
    title = models.CharField(max_length=80)
    price = models.PositiveIntegerField(
        validators=[
            MaxValueValidator(100000),
            MinValueValidator(1)
        ])
    published_at = models.DateTimeField()
    author = models.CharField(max_length=20)
    page_count = models.PositiveSmallIntegerField()
    description = models.TextField()
    category = models.PositiveSmallIntegerField(choices=BOOK_CATEGORY_CHOICE)

    objects = ProductManager()

    def get_absolute_url(self):
        return reverse('detail', kwargs={'id': self.pk})

    def __str__(self):
        return self.title

    def shortTitle(self):
        return self.title[:25] + '…'

    def middleTitle(self):
        if len(self.title) > 40:
            return self.title[:40] + '…'
        else:
            return self.title

    def categoryName(self):
        return BOOK_CATEGORY_CHOICE[self.category][1]

class Comment(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=30)
    rate = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ]
    )
    body = models.TextField()
