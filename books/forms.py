from django import forms
from .models import Product


class ProductForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(ProductForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Product
        fields = ("title", "author", "price", "page_count", "category", "published_at", "description")
        labels = {
            "title": "題名",
            "author": "著者",
            "price": "価格",
            "page_count": "ページ数",
            "category": "カテゴリ",
            "published_at": "出版日",
            "description": "本の詳細",
        }
        widgets = {
            "published_at": forms.SelectDateWidget
        }
        help_texts = {
            'title': '80字以内',
            "author": "20字以内",
            "price": "10万円以下",
        }
