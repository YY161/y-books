from django.urls import path
from . import views
from .views import AddBookView

urlpatterns = [
    path('', views.index, name='index'),
    path('seed', views.seed, name='seed'),
    path('book_<int:id>', views.detail, name='detail'),
    path('search', views.search, name='search'),
    path('add_book', AddBookView.as_view(), name='add_book'),
]
